Changelog
=========

Version 1.2.1
-------------

- [#4](https://gitlab.com/maniascript/antlr-docker/-/issues/4) Fix node integration

Version 1.2.0
-------------

- [#4](https://gitlab.com/maniascript/antlr-docker/-/issues/4) Add node to the image